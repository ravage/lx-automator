import datetime
import os
import uuid
from automator import paths
from jinja2 import Environment, FileSystemLoader

path = os.path.dirname(os.path.abspath(__file__))
path = os.path.join(path, 'templates')
env = Environment(loader=FileSystemLoader(path))


def render(filename, data, subdirectory=None):
    """
    Render a template placing the resulting file in CONFIG_DIR.

    :param filename: string, template filename
    :param data: dict, context for template value replacement
    :param subdirectory: string, if given template will be placed in CONFIG_DIR/subdirectory
    :return: string, full path to the rendered template
    """
    template = env.get_template(filename)
    output = template.render(**data)
    if subdirectory is not None:
        template_full_path = os.path.join(paths.CONFIG_DIR, subdirectory, generate_filename())
    else:
        template_full_path = os.path.join(paths.CONFIG_DIR, generate_filename())

    with open(template_full_path, 'wb') as f:
        f.write(output)

    return template_full_path


def generate_filename():
    """
    Generate random filename for each render
    :return: random timestamped string
    """
    now = datetime.datetime.today().strftime('%Y%m%d%H%M%S')
    return '{}-{}'.format(now, uuid.uuid4().hex)


def wrap_quotes(values, sep, attribute=None):
    """
    Jinja filter to wrap attributes in single quotes while separating them by
    a character defined in *sep*.
    :param values: list of elements to wrap
    :param sep: character for element separation
    :param attribute: if *values* contains objects, define which member to grab
    :return: wrapped string
    """
    if attribute:
        result = sep.join(["'{}'".format(getattr(value, attribute)) for value in values])
    else:
        result = sep.join(["'{}'".format(value) for value in values])

    return result

env.filters['wrap_quotes'] = wrap_quotes
