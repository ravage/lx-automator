# discover fixed locations necessary for program operation

import os

CONFIG_DIR = os.path.join(os.path.expanduser('~'), '.leanxcale')
ROOT = os.path.dirname(os.path.abspath(__file__))
META = 'meta.yml'
