import logging
import os
import shutil
import boto3
import botocore
from automator import (
    templates,
    utils,
    paths,
    commands,
    metadata
)

formatter = logging.Formatter(
    '[%(asctime)s] %(name)s %(levelname)s %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S"
)

handler = logging.StreamHandler()
handler.setFormatter(formatter)
logger = logging.getLogger(__name__)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

DRY_RUN_OPERATION = 'DryRunOperation'


class AWS(object):
    """
    LeanXcale AWS provisioning class using boto3 (official AWS API) containing
    methods to support infrastructure and services deployment.

    The run_leanxcale_setup method provides all the steps necessary for a
    working deployment. It encapsulates and demonstrates which methods should
    get called and how.
    """
    def __init__(self, cfg):
        self.ec2 = boto3.resource('ec2')
        self.client = boto3.client('ec2')
        self.cfg = cfg
        self.meta = metadata.Metadata(self.metadata_path)

    def get_error_code(self, response):
        """
        Extract error code from AWS response

        :param response: AWS response dictionary
        :return: a string representing the error code
        """
        return response['Error']['Code']

    def get_private_key_path(self):
        """
        Build private key path from configuration

        :return: full path to private key
        """
        key_name = '{}.pem'.format(self.cfg.remote.key_name)
        return os.path.join(self.cluster_path, key_name)

    def get_private_keys(self):
        """
        Pull private key from AWS by name

        :return: resource representing an ec2.KeyPair
        """
        return self.ec2.key_pairs.filter(
            Filters=[
                {
                    'Name': 'key-name',
                    'Values': [self.key_name]
                },
            ]
        )

    def create_private_key(self):
        """
        Pull and store AWS private key in local filesystem

        :return: full path to the private key
        """
        logger.info('Creating Private Key')
        keys = list(self.get_private_keys())

        if len(keys) > 0:
            keys[0].delete()

        key = self.ec2.create_key_pair(KeyName=self.key_name)

        key_path = self.get_private_key_path()

        with open(key_path, 'w') as f:
            f.write(key.key_material)

        os.chmod(key_path, 0600)

        return key_path

    def get_placement_groups(self):
        """
        Get a list of placement groups from AWS by name

        :return: resource representing ec2.PlacementGroup
        """
        return self.ec2.placement_groups.filter(
            Filters=[
                {
                    'Name': 'group-name',
                    'Values': [self.placement_group_name]
                },
            ]
        )

    def create_placement_group(self):
        """
        Create AWS placement group and remove previous ones if they exist
        """
        logger.info('Creating Placement Group')

        pgs = self.get_placement_groups()

        for pg in pgs:
            pg.delete()

        self.ec2.create_placement_group(
            GroupName=self.placement_group_name,
            Strategy='cluster'
        )

    def create_security_group(self, vpc):
        """
        Create AWS security group to manage instances network access

        :param vpc: VPC to which the security group should be associated
        :return: resource representing an ec2.SecurityGroup
        """
        security_group = None

        if not self.security_group_exists(vpc):
            security_group = vpc.create_security_group(
                GroupName=self.security_group_name,
                Description=self.security_group_name
            )

            security_group.authorize_ingress(
                IpPermissions=[
                    {
                        'IpProtocol': '-1',
                        'IpRanges': [
                            {
                                'CidrIp': '0.0.0.0/0'
                            },
                        ]
                    }
                ]
            )

        return security_group

    def security_group_exists(self, vpc):
        """
        Check if a given security group is already defined for a VPC

        :param vpc: VPC to list security goups from
        :return: resource representing an ec2.SecurityGroup or None if no security group if found
        """
        sgs = vpc.security_groups.all()
        security_group = None

        for sg in sgs:
            if sg.group_name == self.security_group_name:
                security_group = sg
                break

        return security_group

    def provision_network(self):
        """
        Setup the network components and dump the resulting ids into a metadata
        file for later reference and teardown, this includes:
            - VPC
            - NAT Gateway
            - Internet Gateway
            - Security Group
            - Public and private subnets
            - Elastic IP
            - Routing tables for public and private subnet
        """
        logger.info('Starting Network Setup')

        # NAT gateway takes a while to build, prepare a waiter for it
        nat_gateway_waiter = self.client.get_waiter('nat_gateway_available')

        # VPC will hold public and private subnet blocks
        logger.info('Creating VPC')
        vpc = self.ec2.create_vpc(CidrBlock=self.cfg.vpc.subnet)

        # LeanXcale (wide open) Security Group
        logger.info('Creating Security Group')
        security_group = self.create_security_group(vpc)

        # subnet for internet facing instances
        logger.info('Creating Public Network')
        public_subnet = vpc.create_subnet(CidrBlock=self.cfg.gateway.subnet)

        # subnet for private instances with internet access
        logger.info('Creating Private Network')
        private_subnet = vpc.create_subnet(CidrBlock=self.cfg.node.subnet)

        # gateway for public instances
        logger.info('Creating Gateway')
        gateway = self.ec2.create_internet_gateway()
        gateway.attach_to_vpc(VpcId=vpc.id)

        # NAT gateway needs an elastic IP address
        # reuse ips or check for free ones
        logger.info('Allocating IPs')
        elastic_ip = self.client.allocate_address(Domain='vpc')

        # NAT gateway will grant private instances internet access
        logger.info('Creating NAT Gateway (may take a while)')
        nat_gateway = self.client.create_nat_gateway(
            SubnetId=public_subnet.id,
            AllocationId=elastic_ip['AllocationId']
        )
        nat_gateway_id = nat_gateway['NatGateway']['NatGatewayId']
        nat_gateway_waiter.wait(NatGatewayIds=[nat_gateway_id])

        # route public instances via internet gateway
        logger.info('Preparing Routing Tables')
        for route_table in vpc.route_tables.all():
            route_table.create_route(
                DestinationCidrBlock='0.0.0.0/0',
                GatewayId=gateway.id
            )
            route_table.associate_with_subnet(SubnetId=public_subnet.id)

        # route private instances via NAT gateway
        secondary_route_table = vpc.create_route_table()
        secondary_route_table.create_route(
            DestinationCidrBlock='0.0.0.0/0',
            GatewayId=nat_gateway_id
        )
        secondary_route_table.associate_with_subnet(
            SubnetId=private_subnet.id
        )

        result = {
            'gw_subnet': public_subnet,
            'nat_subnet': private_subnet,
            'pub_security_group': security_group,
            'priv_security_group': security_group,
            'vpc': vpc,
            'elastic_ip': elastic_ip,
            'nat_gateway': nat_gateway,
            'gateway': gateway,
            'routing': secondary_route_table
        }

        self.meta.dump(result)

        logger.info('Network Setup Complete')

    def set_instance_attributes(self, instances, attrs=[], stop=True):
        """
        Set attributes for a given instance. Some can be set while the instance is
        running, others require a stopped instance. This method manages the lifecycle
        of the instance accordingly to given parameters. The attributes must be set one by one.

        :param instances: list of instance to be changed
        :param attrs: list of functions to call for setting the attributes
        :param stop: flag indicating if the instances should be stopped before setting the attributes
        """
        waiter_stop = self.client.get_waiter('instance_stopped')
        waiter_running = self.client.get_waiter('instance_running')
        ids = [instance.id for instance in instances]

        if stop:
            self.client.stop_instances(InstanceIds=ids)
            waiter_stop.wait(InstanceIds=ids)

        for fn in attrs:
            fn(instances)

        if stop:
            self.client.start_instances(InstanceIds=ids)
            waiter_running.wait(InstanceIds=ids)

    def add_vpn_route(self, instance_id):
        """
        Inserts a route into the private subnet routing table so packets sent
        from the VPN client reach internal hosts and vice-versa.

        :param instance_id: string with VPN instance identifier
        """
        meta = self.meta.load()
        route_table = self.ec2.RouteTable(meta.get('routing'))

        route_table.create_route(
            DestinationCidrBlock=self.cfg.vpn.subnet,
            InstanceId=instance_id
        )

    def provision_vpn_instance(self, meta):
        """
        Provision VPN instance on AWS allocating an elastic ip and disabling
        packet source/destination checks (necessary to reach private subnet).

        Tags are also applied, see vpn_tag and see leanxcale_tag.

        :param meta: metadata containing network setup information
        """
        logger.info('Provisioning VPN Instance')
        instances = self.ec2.create_instances(
            ImageId=self.cfg.gateway.image_id,
            MinCount=1,
            MaxCount=1,
            KeyName=self.key_name,
            InstanceType=self.cfg.gateway.instance_type,
            NetworkInterfaces=[
                {
                    'DeviceIndex': 0,
                    'Groups': [meta.get('pub_security_group')],
                    'SubnetId': meta.get('gw_subnet'),
                    'AssociatePublicIpAddress': True
                }
            ]
        )

        self.wait_until_running(instances)

        self.tag_instances(
            instances,
            [
                {'Key': 'Type', 'Value': self.vpn_tag},
                {'Key': 'Mark', 'Value': self.leanxcale_tag}
            ]
        )

        self.add_vpn_route((instances)[0].id)

        self.set_instance_attributes(
            instances,
            attrs=[self.disable_source_dest_check],
            stop=False
        )

    def tag_instances(self, instances, tags):
        """
        Apply a set of tags to a given set of instances.

        List of tags format:
        [
            {'Key': 'Type', 'Value': 'Node'},
            {'Key': 'Mark', 'Value': 'LeanXcale'}
        ]

        :param instances: list with ec2.Instance resources
        :param tags: list of tags in AWS API expected format
        """
        for instance in instances:
            instance.create_tags(Tags=tags)

    def provision_leanxcalce_instances(self, meta):
        """
        Provision Node instances on AWS with optimized block storage (EbsOptimized)
        and custom storage block settings (BlockDeviceMappings).

        Tags are also applied, see node_tag and see leanxcale_tag.

        :param meta: metadata containing network setup information
        """
        logger.info('Provisioning Cluster Nodes Instances')
        instances = self.ec2.create_instances(
            ImageId=self.cfg.node.image_id,
            MinCount=self.cfg.node.number,
            MaxCount=self.cfg.node.number,
            KeyName=self.key_name,
            InstanceType=self.cfg.node.instance_type,
            Placement={
                'GroupName': self.placement_group_name,
            },
            EbsOptimized=True,
            BlockDeviceMappings=[
                {
                    'DeviceName': '/dev/sda1',
                    'Ebs': {
                        'VolumeSize': self.cfg.node.volume_size,
                        'DeleteOnTermination': True,
                        'VolumeType': self.cfg.node.volume_type,
                        'Iops': self.cfg.node.iops,
                    },
                }
            ],
            NetworkInterfaces=[
                {
                    'DeviceIndex': 0,
                    'Groups': [meta.get('priv_security_group')],
                    'SubnetId': meta.get('nat_subnet'),
                    'AssociatePublicIpAddress': False
                }
            ]
        )

        self.wait_until_running(instances)
        self.tag_instances(
            instances,
            [
                {'Key': 'Type', 'Value': self.node_tag},
                {'Key': 'Mark', 'Value': self.leanxcale_tag}
            ]
        )

    def enable_sriov(self, instances):
        """
        Attribute setting function to enable enhanced networking for use with
        set_instance_attributes.

        :param instances: list of ec2.Instance resources
        """
        for instance in instances:
            instance.modify_attribute(SriovNetSupport={'Value': 'simple'})

    def disable_source_dest_check(self, instances):
        """
        Attribute setting function to disable source/dest check, for use with
        set_instance_attributes.

        :param instances: list of ec2.Instance resources
        """
        for instance in instances:
            instance.modify_attribute(SourceDestCheck={'Value': False})

    def wait_until_running(self, instances):
        """
        Helper function to call waiters on a list of ec2.Instance resources.

        :param instances: list of ec2.Instance resources
        """
        for instance in instances:
            instance.wait_until_running()

    def get_instances_by_tags(self, *tags):
        """
        Filter instances by given tag values.

        :param tags: list of strings with tag values to use as filter
        :return: list of ec2.Instance resources
        """
        instances = self.ec2.instances.filter(
            Filters=[
                {
                    'Name': 'instance-state-name',
                    'Values': ['running']
                },
                {
                    'Name': 'tag-value',
                    'Values': tags
                }
            ]
        )

        return list(instances)

    def get_nat_gateway(self, vpc_id):
        """
        Get provisioned NAT Gateway id for a given VPC.

        :param vpc_id: string identified the provisioned VPC
        :return: string with NAT Gateway id or None if no NAT Gateway is found
        """
        response = self.client.describe_nat_gateways(
            Filter=[
                {
                    'Name': 'vpc-id',
                    'Values': [vpc_id],
                },
            ],
        )

        gateways = response.get('NatGateways')
        return gateways[0].get('NatGatewayId') if len(gateways) > 0 else None

    def render_templates(self, **kwargs):
        """
        Render all necessary templates into ~/.leanxcale/<cluster_name>:
            - hosts: /etc/hosts dor DNS resolution
            - cluster.py: script to execute on private subnet nodes
            - nodes.yml: LeanXcale environment configuration

        Keyword Arguments:
            nodes -- list of nodes ip address
            vpn   -- list of vpn ip address

        Returned dict Keys:
            hosts_file              -- renderer /etc/hosts template
            cluster_script          -- rendered cluster.py script
            leanxcale_environment   -- rendered LeanXcale environment

        :param kwargs: context for templates
        :return: dict containing the path to the rendered template
        """
        nodes = kwargs.get('nodes')
        vpn = kwargs.get('vpn')
        hosts = nodes + vpn

        hosts_file = templates.render(
            'hosts.j2',
            {'hosts': hosts},
            subdirectory=self.cfg.cluster.name
        )

        cluster_script = templates.render(
            'cluster.py.j2',
            {
                'private_key': self.cfg.remote.private_key,
                'user': self.cfg.remote.user,
                'hosts': hosts,
                'automator_folder': self.cfg.remote.automator_folder
            },
            subdirectory=self.cfg.cluster.name
        )

        leanxcale_environment = templates.render(
            'nodes.yaml.j2',
            {
                'user': self.cfg.remote.user,
                'private_key': self.cfg.remote.private_key,
                'hosts': nodes

            },
            subdirectory=self.cfg.cluster.name
        )

        return {
            'hosts_file': hosts_file,
            'cluster_script': cluster_script,
            'leanxcale_environment': leanxcale_environment
        }

    def has_tag(self, instance, tag):
        """
        Check if a given tag value is present in the instance.

        :param instance: ec2.Instance resource
        :param tag: string with the tag value to verify
        :return: True if tag is found, False otherwise
        """
        for instance_tag in instance.tags:
            if instance_tag.get('Value') == tag:
                return True

        return False

    def display_info(self):
        """
        Load generated metadata file containing setup information and
        query existing instances in order to present information regarding
        current setup.

        Displays:
            - Nodes private address
            - VPN public address
            - VPN login and password
            - LeanXcale nodes and deployed services
        """
        if os.path.exists(self.metadata_path):
            instances = self.get_instances_by_tags(self.leanxcale_tag)
            nodes = tuple(instance for instance in instances if self.has_tag(instance, self.node_tag))
            vpns = tuple(instance for instance in instances if self.has_tag(instance, self.vpn_tag))
            meta = self.meta.load()

            logger.info('---------------------------------------------------------------------')
            logger.info('Nodes: %s', ', '.join(node.private_ip_address for node in nodes))
            logger.info('VPN: %s', ', '.join(vpn.public_ip_address for vpn in vpns))
            logger.info('Login / Password: %s / %s', meta.get('username'), meta.get('password'))
            for node, services in meta.get('leanxcale').items():
                logger.info('Node: %s | Services: %s', node, services)
            logger.info('---------------------------------------------------------------------')
        else:
            logger.info('No setup found!')

    def run_leanxcale_setup(self):
        """
        Fully provision a working LeanXcale setup from network to service deployment.

        Provisioning Steps:
            - Private keys
            - Placement group
            - Network stack
            - Node and VPN instances
            - Template rendering
            - Username and password generation
            - LeanXcale deployment

        :return: integer representing success (0) or failure (1)
        """
        self.create_private_key()
        self.create_placement_group()
        self.provision_network()

        network_data = self.meta.load()

        self.provision_leanxcalce_instances(network_data)
        self.provision_vpn_instance(network_data)

        nodes = self.get_instances_by_tags(self.node_tag)
        vpns = self.get_instances_by_tags(self.vpn_tag)
        vpn = vpns[0]

        if len(nodes) == 0 or len(vpns) == 0:
            # TODO: philosophy, early or late return?
            logger.error('No hosts found, terminating')
            return 1

        rendered = self.render_templates(nodes=nodes, vpn=vpns)

        utils.wait_until_ssh_available(
            vpn.public_ip_address,
            self.cfg.remote.user,
            self.get_private_key_path()
        )

        username, password = ('leanxcale', utils.random_string())

        components = self.meta.external(rendered.get('leanxcale_environment'), 'components')
        self.meta.append(username=username, password=password, leanxcale=components)

        commands.go(
            cluster_script=rendered.get('cluster_script'),
            utils_script=os.path.join(paths.ROOT, 'utils.py'),
            leanxcale_environment=rendered.get('leanxcale_environment'),
            hosts_file=rendered.get('hosts_file'),
            private_key=self.get_private_key_path(),
            hosts=vpn.public_ip_address,
            username=username,
            password=password,
            cfg=self.cfg
        )

        return 0

    def destroy(self):
        """
        Teardown current LeanXcale setup terminating instances, services and infrastructure.
        :return: integer representing success (0) or failure (1)
        """
        if not os.path.exists(self.metadata_path):
            logger.info('No previous setup found')
            return 1

        self.meta.load()
        self.destroy_nat_gateway()
        self.destroy_instances()
        self.destroy_eip()
        self.destroy_subnet()
        self.destroy_routing()
        self.destroy_gateway()
        self.destroy_security_groups()
        self.destroy_vpc()
        self.destroy_config()

        return 0

    def destroy_nat_gateway(self):
        # TODO: waiter is bugged, create one
        logger.info('Terminating NAT Gateway')
        nat_gateway_waiter = self.client.get_waiter('nat_gateway_available')
        self.client.delete_nat_gateway(NatGatewayId=self.meta.get('nat_gateway'))
        try:
            nat_gateway_waiter.wait(
                Filters=[
                    {
                        'Name': 'state',
                        'Values': ['deleted']
                    },
                    {
                        'Name': 'nat-gateway-id',
                        'Values': [self.meta.get('nat_gateway')]
                    }
                ],
            )
        except botocore.exceptions.WaiterError:
            logger.info('Skipping NAT Gateway {}'.format(self.meta.get('nat_gateway')))

    def destroy_instances(self):
        logger.info('Terminating Instances')
        instances = [instance.id for instance in self.get_instances_by_tags(self.leanxcale_tag)]

        if len(instances) > 0:
            waiter = self.client.get_waiter('instance_terminated')
            self.client.terminate_instances(InstanceIds=instances)
            waiter.wait(InstanceIds=instances)

    def destroy_eip(self):
        logger.info('Deleting Elastic IPs')
        allocation_id = self.meta.get('elastic_ip')

        try:
            self.client.release_address(AllocationId=allocation_id)
        except botocore.exceptions.ClientError:
            logger.info('Skipping EIP {}'.format(allocation_id))

    def destroy_vpc(self):
        logger.info('Deleting VPC')
        try:
            self.client.delete_vpc(VpcId=self.meta.get('vpc'))
        except botocore.exceptions.ClientError:
            logger.info('Skipping VPC {}'.format(self.meta.get('vpc')))

    def destroy_subnet(self):
        logger.info('Deleting Subnets')
        subnets = set()
        subnets.add(self.meta.get('gw_subnet'))
        subnets.add(self.meta.get('nat_subnet'))

        for subnet_id in subnets:
            subnet = self.ec2.Subnet(subnet_id)
            try:
                subnet.delete()
            except botocore.exceptions.ClientError:
                logger.info('Skipping Subnet {}'.format(subnet_id))

    def destroy_routing(self):
        logger.info('Deleting Routing Tables')
        routing = self.ec2.RouteTable(self.meta.get('routing'))
        try:
            routing.delete()
        except botocore.exceptions.ClientError:
            logger.info('Skipping Routing Table {}'.format(self.meta.get('routing')))

    def destroy_gateway(self):
        gateway = self.ec2.InternetGateway(self.meta.get('gateway'))
        try:
            logger.info('Deleting Internet Gateway')
            gateway.detach_from_vpc(VpcId=self.meta.get('vpc'))
            gateway.delete()
        except botocore.exceptions.ClientError:
            logger.info('Skipping Internet Gateway {}'.format(self.meta.get('gateway')))

    def destroy_security_groups(self):
        logger.info('Deleting Security Groups')
        sg_ids = set()
        sg_ids.add(self.meta.get('pub_security_group'))
        sg_ids.add(self.meta.get('priv_security_group'))

        for sg_id in sg_ids:
            try:
                sg = self.ec2.SecurityGroup(sg_id)
                sg.delete()
            except botocore.exceptions.ClientError:
                logger.info('Skipping Security Group {}'.format(sg_id))

    def destroy_config(self):
        shutil.rmtree(self.cluster_path)

    @property
    def security_group_name(self):
        """
        Build security group name.
        :return: string
        """
        return 'LeanXcale-{}'.format(self.cfg.cluster.name)

    @property
    def key_name(self):
        """
        Build private key name.
        :return: string
        """
        return '{}-{}'.format(self.cfg.remote.key_name, self.cfg.cluster.name)

    @property
    def vpn_tag(self):
        """
        Build tag for VPN instance.
        :return: string
        """
        return 'VPN-{}'.format(self.cfg.cluster.name)

    @property
    def node_tag(self):
        """
        Build tag for node instances.
        :return: string
        """
        return 'Node-{}'.format(self.cfg.cluster.name)

    @property
    def leanxcale_tag(self):
        """
        Build LeanXcale instances tag name.
        :return: string
        """
        return 'LeanXcale-{}'.format(self.cfg.cluster.name)

    @property
    def metadata_path(self):
        """
        Build full path to metadata file.
        :return: string
        """
        return os.path.join(self.cluster_path, paths.META)

    @property
    def cluster_path(self):
        """
        Build full path to environment folder ~/.leanxcale/<environment>.
        :return: string
        """
        return os.path.join(paths.CONFIG_DIR, self.cfg.cluster.name)

    @property
    def placement_group_name(self):
        """
        Build placement group name.
        :return: string
        """
        return '{}-{}'.format(self.cfg.node.placement_group, self.cfg.cluster.name)
