# command line flag definitions

import argparse

parser = argparse.ArgumentParser()

parser.add_argument(
    '-c',
    required=True,
    help='path to configuration file',
    metavar='config.yml',
    type=argparse.FileType('r'),
    dest='config'
)

parser.add_argument(
    '-d',
    help='destroy LeanXcale setup',
    dest='destroy',
    action='store_true'
)

parser.add_argument(
    '-i',
    help='display setup information',
    dest='info',
    action='store_true'
)
