import yaml
from collections import namedtuple


def make(data):
    """
    Transform dict into namedtuple in order to access key values with dot notation

    :param data: single depth dict
    :return: namedtuple representing loaded configuration
    """
    items = []

    for k, v in data.items():
        items.append(namedtuple(k.title(), v.keys())(**v))

    return namedtuple('Config', data.keys())(*items)


def load(f):
    # load yaml file into Python dict and transform it into a namedtuple
    return make(yaml.safe_load(f))
