import paramiko
import random
import string

from paramiko.ssh_exception import (
    SSHException,
    ChannelException,
    NoValidConnectionsError
)
import time


def wait_until_ssh_available(host, user, pk):
    # TODO: implement timeout
    """
    Verify ssh access for a given hosts by logging in and issuing a command.
    Block execution and retry until success.

    :param host: string, ip address or hostname to connect
    :param user: string, username for connection
    :param pk: string, private key for authentication
    """

    retry = True
    ssh = paramiko.SSHClient()
    while retry:
        try:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(
                host,
                username=user,
                key_filename=pk
            )
            ssh.exec_command('ls -l')
            retry = False
        except (
            SSHException,
            ChannelException,
            NoValidConnectionsError
        ):
            time.sleep(5)
        finally:
            ssh.close()


def aws_len(collection):
    """
    AWS resource collections do not support *len* method, this utility provides
    functionality if necessary.

    :param collection: AWS resource collection
    :return: int, collection length
    """
    return sum(1 for _ in collection)


def random_string(length=8):
    """
    Generate random alphanumeric string of given length

    :param length: int, string size
    :return: random alphanumeric string
    """
    chars = string.ascii_letters + string.digits
    result = ''.join(random.SystemRandom().choice(chars) for _ in range(length))
    return result
