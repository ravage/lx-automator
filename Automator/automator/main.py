import os
import shutil
import sys
import logging
import logging.handlers

sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
from automator import (arguments, aws, paths, config)  # noqa

if __name__ == '__main__':
    formatter = logging.Formatter(
        '[%(asctime)s] %(name)s %(levelname)s %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
    )

    handler = logging.handlers.RotatingFileHandler('leanxcale.log', maxBytes=1000000, backupCount=5)
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    args = arguments.parser.parse_args()
    cfg = config.load(args.config)
    cluster_config_path = os.path.join(paths.CONFIG_DIR, cfg.cluster.name)

    if not os.path.exists(cluster_config_path):
        os.makedirs(cluster_config_path, 0700)

    shutil.copyfile(
        args.config.name,
        os.path.join(cluster_config_path, 'config.yml')
    )

    args.config.close()

    client = aws.AWS(cfg)
    result = 0

    if args.destroy:
        result = client.destroy()
    elif args.info:
        client.display_info()
    else:
        result = client.run_leanxcale_setup()
        if result == 0:
            print(client.display_info())

    sys.exit(result)
