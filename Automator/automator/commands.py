# fabric tasks for use in final setup stage and service management

from fabric.operations import (put, run, sudo)
from fabric.api import (hide, env)
from fabric.tasks import execute
from fabric.contrib import files
from fabric.context_managers import cd
import os

env.connection_attempts = 3
env.command_timeout = 90


def rewrite_remote_etc_hosts(cfg):
    script = os.path.join(cfg.remote.automator_folder, 'cluster.py')
    command = 'fab -f {} rewrite_etc_hosts'.format(script)
    with hide('output', 'running'):
        run(command)


def create_scripts_folder(cfg):
    if not files.exists(cfg.remote.automator_folder):
        command = 'mkdir {}'.format(cfg.remote.automator_folder)
        with hide('output', 'running'):
            run(command)


def upload_cluster_script(filename, cfg):
    destination = os.path.join(cfg.remote.automator_folder, 'cluster.py')
    with hide('output', 'running'):
        put(filename, destination)


def upload_leanxcale_environment(filename, cfg):
    with hide('output', 'running'):
        destination = os.path.join(cfg.remote.automator_folder, 'leanxcale.yml')
        put(filename, destination)


def upload_hosts_file(filename, cfg):
    with hide('output', 'running'):
        destination = os.path.join(cfg.remote.automator_folder, 'hosts')
        put(filename, destination)


def upload_utils_script(filename, cfg):
    with hide('output', 'running'):
        destination = os.path.join(cfg.remote.automator_folder, 'utils.py')
        put(filename, destination)


def upload_private_key(filename, cfg):
    with hide('output', 'running'):
        destination = cfg.remote.private_key
        put(filename, destination, mode='0600')


def check_hosts_availability(cfg):
    script = os.path.join(cfg.remote.automator_folder, 'cluster.py')
    command = 'fab -f {} check_hosts_availability'.format(script)
    with hide('output', 'running'):
        run(command, timeout=900)


def run_leanxcale_installer(cfg):
    config_file = os.path.join(cfg.remote.automator_folder, 'leanxcale.yml')
    command = 'python install.py -p {} --skip-validation'.format(config_file)
    with cd(cfg.remote.distribution_path), hide('running'):
        run(command, timeout=900)


def run_leanxcale_start(cfg):
    config_file = os.path.join(cfg.remote.automator_folder, 'leanxcale.yml')
    command = 'python start.py -p {} --skip-validation'.format(config_file)
    with cd(cfg.remote.distribution_path), hide('running'):
        run(command, timeout=900)


def touch_knonwn_hosts():
    with hide('output', 'running'):
        run('touch ~/.ssh/known_hosts')


def manage_vpn_user(username, password, gecos=''):
    with hide('output', 'running'):
        if files.contains('/etc/passwd', username):
            sudo('userdel -r {}'.format(username))

        sudo('adduser {} --disabled-password --gecos {}'.format(username, gecos))
        sudo('echo {}:{} | chpasswd'.format(username, password))


def go(**kwargs):
    cfg = kwargs.get('cfg')

    env.key_filename = kwargs.get('private_key')
    env.user = cfg.remote.user
    env.hosts = kwargs.get('hosts')

    execute(create_scripts_folder, cfg)
    execute(touch_knonwn_hosts)
    execute(upload_hosts_file, kwargs.get('hosts_file'), cfg)
    execute(upload_cluster_script, kwargs.get('cluster_script'), cfg)
    execute(upload_utils_script, kwargs.get('utils_script'), cfg)
    execute(upload_leanxcale_environment, kwargs.get('leanxcale_environment'), cfg)
    execute(upload_private_key, kwargs.get('private_key'), cfg)
    execute(check_hosts_availability, cfg)
    execute(rewrite_remote_etc_hosts, cfg)
    execute(manage_vpn_user, kwargs.get('username'), kwargs.get('password'), 'LeanXcale')
    execute(run_leanxcale_installer, cfg)
    execute(run_leanxcale_start, cfg)
