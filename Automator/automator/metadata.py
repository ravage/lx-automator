import yaml


class Metadata(object):
    """
    Provide API to load, dump and access metadata yaml file containing setup information.
    The metadata file contains provision data gathered during setup, it's crucial for
    teardown process, but also, for running a step by step installation.
    """

    def __init__(self, source):
        """
        :param source: location for the metadata file
        """
        self._source = source
        self._data = {}

    @property
    def source(self):
        """
        Accessor for _source property

        :return: string with the location for metadata file
        """
        return self._source

    def dump(self, meta):
        # TODO: metadata should probably come already parsed?
        """
        Parse and dump dict with network setup into metadata

        :param meta: dict with required and initial setup.
        """
        with open(self.source, 'w') as writer:
            yaml.dump({
                'gw_subnet': meta.get('gw_subnet').id,
                'nat_subnet': meta.get('nat_subnet').id,
                'pub_security_group': meta.get('pub_security_group').id,
                'priv_security_group': meta.get('priv_security_group').id,
                'vpc': meta.get('vpc').id,
                'elastic_ip': meta.get('elastic_ip').get('AllocationId'),
                'nat_gateway': meta.get('nat_gateway').get('NatGateway').get('NatGatewayId'),
                'gateway': meta.get('gateway').id,
                'routing': meta.get('routing').id
            }, writer, default_flow_style=False)

    def load(self):
        """
        Load metadata file if no cache is present returning a dict representation

        :return: dict representing gathered metadata
        """
        if bool(self._data):
            return self

        with open(self.source, 'r') as reader:
            self._data = yaml.safe_load(reader)

        return self

    def append(self, **kwargs):
        """
        Append metadata to an already existing file, also updating local cache.
        Duplicate keys will be overwritten.

        :param kwargs: dict with new metadata
        """
        self.load()
        self._data.update(kwargs)

        with open(self.source, 'w') as writer:
            yaml.dump(self._data, writer, default_flow_style=False)

    def get(self, key):
        """
        Getter for local metadata cache.

        :param key: string with lookup key
        :return: value from local cache dictionary
        """
        return self._data.get(key)

    def __getitem__(self, key):
        """
        Indexer for local metadata cache.

        :param key: string with lookup key
        :return: value from local cache dictionary
        """
        return self._data.get(key)

    def external(self, source, key):
        """
        Utility method for loading arbitrary yaml file and returning the value associated with a given key.

        :param source: string, path to file
        :param key: string, key to return from parsed dict
        :return: mixed, value or None if key does not exist
        """
        with open(source, 'r') as reader:
            doc = yaml.safe_load(reader)

        return doc.get(key)
