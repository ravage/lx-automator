#!/bin/bash -ex
user=deployer
gecos=Deployer
usermod  -l $user ubuntu
groupmod -n $user ubuntu
usermod  -d /home/$user -m $user
if [ -f /etc/sudoers.d/90-cloudimg-ubuntu ]; then
  mv /etc/sudoers.d/90-cloudimg-ubuntu /etc/sudoers.d/90-cloud-init-users
fi
perl -pi -e "s/ubuntu/$user/g;" /etc/sudoers.d/90-cloud-init-users
perl -pi -e "s/name: ubuntu/name: $user/g;" /etc/cloud/cloud.cfg
perl -pi -e "s/Ubuntu/$gecos/g;" /etc/cloud/cloud.cfg